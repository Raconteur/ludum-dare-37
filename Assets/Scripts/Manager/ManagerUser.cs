﻿using UnityEngine;
using System.Collections;
//using SaFrLib;

/// ManagerUser
/// A one-sentence description of what this script does
///
/// WHO
///     [This section explains which programmers and/or in-game entities use this class]
/// 
/// WHAT
///     [This section explains what this class does. Expand on the single-sentence summary from earlier.]
///
/// HOW
///     [This section explains how to implement the class, using numbered steps.]
///
/// WHY
///     [This section explains why a class exists, if that reason isn't readily apparent.]
///
public class ManagerUser : MonoBehaviour {

	public GameManager manager {
		get {
			if (_manager == null) {
				_manager = FindObjectOfType<GameManager> ();
			}
			return _manager;
		}
		set {
			_manager = value;
		}
	}
	protected GameManager _manager;

}