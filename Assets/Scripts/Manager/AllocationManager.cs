﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using SaFrLib;

/// AllocationManager
/// A one-sentence description of what this script does
///
/// WHO
///     [This section explains which programmers and/or in-game entities use this class]
/// 
/// WHAT
///     [This section explains what this class does. Expand on the single-sentence summary from earlier.]
///
/// HOW
///     [This section explains how to implement the class, using numbered steps.]
///
/// WHY
///     [This section explains why a class exists, if that reason isn't readily apparent.]
///
public class AllocationManager : ManagerUser {

	[System.Serializable]
	public struct AllocationArea {
		public string name;
		public Image bar;
		public Image[] pointIndicator;
		public int pointsAllocated;
		public Button increment, decrement;
	}

	public int pointsToAllocate = 5;
	private int pointsBeingUsed = 0;
	public AllocationArea[] allocationAreas;

	void Start() {
		foreach (AllocationArea a in allocationAreas) {
			SetupButtons (a);
		}
	}

	void SetupButtons(AllocationArea a) {
		a.increment.onClick.AddListener (() => {
			if ( pointsBeingUsed < pointsToAllocate && a.pointsAllocated < a.pointIndicator.Length) {
				a.pointsAllocated++;
				pointsBeingUsed++;
				RefreshIndicators(a);
			} else {
				Debug.Log("Not enough points to allocate");
			}
		});

		a.decrement.onClick.AddListener (() => {
			if (a.pointsAllocated > 0) {
				a.pointsAllocated--;
				pointsBeingUsed--;
				RefreshIndicators(a);
			} else {
				Debug.Log("Already at 0 points");
			}
		});
	}

	void RefreshIndicators(AllocationArea a) {
		for (int i = 0; i < a.pointIndicator.Length; i++) {
			if (i < a.pointsAllocated) {
				a.pointIndicator [i].color = Color.green;
			} else {
				a.pointIndicator [i].color = Color.white;
			}
		}
	}
}