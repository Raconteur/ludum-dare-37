﻿using UnityEngine;
using System.Collections;
//using SaFrLib;

/// SceneActor
/// A one-sentence description of what this script does
///
/// WHO
///     [This section explains which programmers and/or in-game entities use this class]
/// 
/// WHAT
///     [This section explains what this class does. Expand on the single-sentence summary from earlier.]
///
/// HOW
///     [This section explains how to implement the class, using numbered steps.]
///
/// WHY
///     [This section explains why a class exists, if that reason isn't readily apparent.]
///
public class SceneActor : ManagerUser {

	public bool atCounter = false;
	public float speed = 0.3f;

	public void SnapToPoint(Transform point) {
		transform.position = point.position;
	}

	public void StopAnimation() {
		if (name == "Death")
			return;
		GetComponent<Animator> ().enabled = false;
	}

	public void WalkToPoint(Transform point) {
		iTween.Stop(gameObject);

		if (name != "Death") {
			GetComponent<Animator> ().SetTrigger ("WalkDown");
			GetComponent<Animator> ().enabled = true;
		}

		iTween.MoveTo (gameObject, new Hashtable () {
			{ iT.MoveTo.position, point.position },
			{ iT.MoveTo.speed, speed },
			{ iT.MoveTo.easetype, iTween.EaseType.linear },
			{ iT.MoveTo.oncomplete, "StopAnimation" },
			{ iT.MoveTo.oncompletetarget, gameObject }
		});
	}

	public void WalkToDoor() {
		iTween.Stop(gameObject);
		GetComponent<Animator> ().enabled = true;

		GetComponent<Animator> ().SetTrigger ("WalkUp");

		iTween.MoveTo (gameObject, new Hashtable () {
			{ iT.MoveTo.position, manager.door.position },
			{ iT.MoveTo.speed, speed },
			{ iT.MoveTo.easetype, iTween.EaseType.linear },
			{ iT.MoveTo.oncomplete, "Deactivate" },
			{ iT.MoveTo.oncompletetarget, gameObject }
		});
	}

	public void Deactivate() {
		gameObject.SetActive (false);
	}

	public void MoveToCounter() {
		iTween.MoveTo (gameObject, new Hashtable () {
			{ iT.MoveTo.position, new Vector3(manager.player.transform.position.x, manager.counterWaitingZone.position.y) },
			{ iT.MoveTo.speed, speed },
			{ iT.MoveTo.oncomplete, "OnArrivedAtCounter" },
			{ iT.MoveTo.oncompletetarget, gameObject }
		});
	}

	public void OnArrivedAtCounter() {
		
	}

}