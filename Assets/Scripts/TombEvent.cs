﻿using UnityEngine;
using System.Collections;
//using SaFrLib;

/// TombEvent
/// A one-sentence description of what this script does
///
/// WHO
///     [This section explains which programmers and/or in-game entities use this class]
/// 
/// WHAT
///     [This section explains what this class does. Expand on the single-sentence summary from earlier.]
///
/// HOW
///     [This section explains how to implement the class, using numbered steps.]
///
/// WHY
///     [This section explains why a class exists, if that reason isn't readily apparent.]
///
public class TombEvent : ManagerUser {

	public float fillTrigger = 0.5f;
	public GameObject sceneHolder;
	public DialogueCell firstCell;
	public GameObject toEnable;
	public bool started = false;

	public void ActivateEvent(bool forceActivation = false) {
		if (started && !forceActivation)
			return;

		manager.LoadScene (sceneHolder);
		toEnable.SetActive (true);
		manager.dialogueDisplay.SetDisplay (firstCell);
		manager.timeScale = 0;
	}

}