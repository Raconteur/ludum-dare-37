﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
//using SaFrLib;

/// Tester
/// A one-sentence description of what this script does
///
/// WHO
///     [This section explains which programmers and/or in-game entities use this class]
/// 
/// WHAT
///     [This section explains what this class does. Expand on the single-sentence summary from earlier.]
///
/// HOW
///     [This section explains how to implement the class, using numbered steps.]
///
/// WHY
///     [This section explains why a class exists, if that reason isn't readily apparent.]
///
public class Tester : ManagerUser {

	public GameObject text;
	string[] scenes = new string[] {
		"SCENE - Opening",
		"Time Scene 1",
		"Time Scene 2",
		"Final Scene"
	};
	int current = 0;

	public void Interlude() {
		text.SetActive (true);
	}

	public void TimeoutThenCredits() {
		Invoke ("GoToCredits", 2f);
	}
	public UnityEvent fadeToCredits, startCredits, resetEvent;

	public void GoToCredits() {
		fadeToCredits.Invoke ();
		Invoke ("StartCredits", 4f);
	}

	public void StartCredits() {
		startCredits.Invoke ();
	}

	public void TimeoutThenSpaceToContinue() {
		Invoke ("SpaceToContinue", 4f);
	}

	public void SpaceToContinue() {
		resetEvent.Invoke ();
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (text.activeSelf) {
				manager.LoadScene (scenes [current]);
				manager.dialogueDisplay.SetDisplay ("Dialogue Cell");
				current++;
				text.SetActive(false);
			}
		}

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			manager.LoadScene ("SCENE - Opening");
			manager.dialogueDisplay.SetDisplay ("Opening 1");
		}
		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			manager.LoadScene ("Time Scene 1");
			manager.dialogueDisplay.SetDisplay ("Dialogue Cell");
		}
		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			manager.LoadScene ("Time Scene 2");
			manager.dialogueDisplay.SetDisplay ("Dialogue Cell");
		}
		if (Input.GetKeyDown (KeyCode.Alpha4)) {
			manager.LoadScene ("Final Scene");
			manager.dialogueDisplay.SetDisplay ("Dialogue Cell");
		}
		if (Input.GetKeyDown (KeyCode.Alpha5)) {
			manager.LoadScene ("Credits");
			manager.dialogueDisplay.SetDisplay ("Dialogue Cell");
		}
	}
}