﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SaFrLib;

/// GameManager
/// A one-sentence description of what this script does
///
/// WHO
///     [This section explains which programmers and/or in-game entities use this class]
/// 
/// WHAT
///     [This section explains what this class does. Expand on the single-sentence summary from earlier.]
///
/// HOW
///     [This section explains how to implement the class, using numbered steps.]
///
/// WHY
///     [This section explains why a class exists, if that reason isn't readily apparent.]
///
public class GameManager : MonoBehaviour {

	public AllocationManager allocationManager;
	public DialogueDisplay dialogueDisplay;
	protected GameObject currentScene;
	public GameObject player;
	public Transform counterWaitingZone;
	public Transform door;

	public void LoadScene(string scene) {
		currentScene = GameObject.Find (scene);
	}
	public void LoadScene(GameObject scene) {
		currentScene = scene;
	}

	public DialogueCell FindCell(string id) {
		return currentScene ? currentScene.transform.FindChild (id).GetComponent<DialogueCell>() : null;
	}

	#region TimePassing

	public Image timeBar;
	public float yearsToLive = 50f;
	public float secondsForOneYear = 10f;
	public float timeScale = 1f;
	public float tPlusYears = 0;

	public TombEvent[] timeEvents;

	void PassTime() {
		tPlusYears += (1f / secondsForOneYear) * Time.deltaTime * timeScale;
		timeBar.fillAmount = tPlusYears / yearsToLive;
		// Check for time events
		foreach (TombEvent t in timeEvents) {
			if (!t.started && timeBar.fillAmount >= t.fillTrigger) {
				t.ActivateEvent ();
			}
		}
	}

	#endregion

	#region PlayerStats

	public Dictionary<string, float> values = new Dictionary<string, float>();

	public void SetValue(string name, float newValue) {
		if (!values.ContainsKey (name)) {
			values.Add (name, newValue);
		} else {
			values [name] = newValue;
		}
	}
	public float GetValue(string toCheck) {
		if (!values.ContainsKey (toCheck)) {
			values.Add (toCheck, 0);
		}
		return values [toCheck];
	}

	#endregion

	void Update() {
		PassTime ();
	}
}