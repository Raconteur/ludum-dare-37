﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using SaFrLib;

/// DialogueDisplay
/// A one-sentence description of what this script does
///
/// WHO
///     [This section explains which programmers and/or in-game entities use this class]
/// 
/// WHAT
///     [This section explains what this class does. Expand on the single-sentence summary from earlier.]
///
/// HOW
///     [This section explains how to implement the class, using numbered steps.]
///
/// WHY
///     [This section explains why a class exists, if that reason isn't readily apparent.]
///
public class DialogueDisplay : ManagerUser {

	public Text nameDisplay, contentDisplay;
	public GameObject wrapper;
	public GameObject nameDisplayWrapper;
	protected DialogueCell currentCell;

	public void SetDisplay(DialogueCell cell) {
		// TODO: Animate in
		wrapper.SetActive(true);

		currentCell = cell;

		if (cell.conditions.Count <= 0) {
			// Display default content
			if (cell.defaultSpeakerName != "") {
				nameDisplayWrapper.SetActive (true);
				nameDisplay.text = cell.defaultSpeakerName;
			} else {
				nameDisplayWrapper.SetActive (false);
			}
			contentDisplay.text = cell.defaultContent;
			if (cell.defaultOnDisplay != null) {
				cell.defaultOnDisplay.Invoke ();
			}
		} else {
			Debug.Log ("TODO: Branching content");
		}
	}

	public void SetDisplay(string id) {
		SetDisplay (manager.FindCell (id));
	}

	public void Next() {
		if (currentCell.goToBelowCell) {
			int nextIndex = currentCell.transform.GetSiblingIndex () + 1;
			DialogueCell nextCell = currentCell.transform.parent.GetChild (nextIndex).GetComponent<DialogueCell>();
			SetDisplay (nextCell);
		} else if (currentCell.defaultNextCell == "") {
			wrapper.SetActive (false);
		} else {
			SetDisplay (currentCell.defaultNextCell);
		}
	}

}