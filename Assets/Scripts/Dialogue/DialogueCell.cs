﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
//using SaFrLib;

/// DialogueCell
/// A one-sentence description of what this script does
///
/// WHO
///     [This section explains which programmers and/or in-game entities use this class]
/// 
/// WHAT
///     [This section explains what this class does. Expand on the single-sentence summary from earlier.]
///
/// HOW
///     [This section explains how to implement the class, using numbered steps.]
///
/// WHY
///     [This section explains why a class exists, if that reason isn't readily apparent.]
///
public class DialogueCell : MonoBehaviour {

	[System.Serializable]
	public struct Condition {
		public string A;
		public Comparison Is;
		public string B;

		public enum Comparison { LESS_THAN, EQUAL_TO, GREATER_THAN };
	}
	[System.Serializable]
	public struct Router {
		public string speakerName;
		public string content;
		public string nextCellId;
		public UnityEngine.Events.UnityEvent onDisplay;
	}
		
	public string defaultSpeakerName = "";
	public string defaultContent = "";
	public string defaultNextCell = "";
	public UnityEngine.Events.UnityEvent defaultOnDisplay;
	public bool goToBelowCell = false;


	public List<Condition> conditions = new List<Condition>();
	public List<Router> content = new List<Router>();

}